# Shared Queue

This project is a demo for the thread-safe queue.
The project was built with VisualStudio-2019. The solution could be found in the folder .\Projects
You also need GoogleTest package installed in order to build the test project. Release 1.10.0 was used. You can download it from https://github.com/google/googletest/archive/release-1.10.0.zip.
The environment variable GOOGLE_TEST_ROOT must be declared which leads to the root of the google test folder.