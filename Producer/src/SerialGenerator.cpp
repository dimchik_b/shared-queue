#include <stdint.h>
#include "Generator.h"

void* SerialGenerator::get()
{
	static uint32_t counter = 0;
	counter++;
	return new uint32_t(counter);
}