#include "Producer.h"

std::mutex Producer::mutex;

void Producer::produce()
{
	produceTimeout(0);
}

void Producer::produceTimeout(uint32_t timeout_ms)
{
	std::unique_lock<std::mutex> lock(mutex);
	void* data = generator.get();
	lock.unlock();
	queue.enqueue(data, timeout_ms);
}