#ifndef __DATA_GENERATOR__
#define __DATA_GENERATOR__

class DataGenerator
{
public:
	virtual void* get() = 0;
};

class SerialGenerator : public DataGenerator
{
public:
	virtual void* get();
};

#endif __DATA_GENERATOR__
