#ifndef __PRODUCER__
#define __PRODUCER__

#include <mutex>
#include "SharedQueue.h"
#include "Generator.h"

class Producer
{
public:
	Producer(DataGenerator& theGenerator, SharedQueue<void>& theQueue) :
		generator(theGenerator),
		queue(theQueue)
	{}
	~Producer() {};
	void produce();
	void produceTimeout(uint32_t timeout_ms);

private:
	DataGenerator& generator;
	SharedQueue<void>& queue;
	static std::mutex mutex;
};

#endif // __PRODUCER__