#include <stdint.h>
#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "Producer.h"
#include <thread>

using ::testing::_;
using ::testing::Return;
using ::testing::NiceMock;
using ::testing::InSequence;

class MockGenerator : public DataGenerator
{
public:
	MOCK_METHOD(void*, get, (), (override));
};

class MockQueue : public SharedQueue<void>
{
public:
	MOCK_METHOD(int, count, (), (const, override));
	MOCK_METHOD(void, enqueue, (void* item), (override));
	MOCK_METHOD(bool, enqueue, (void* item, int millisecondsTimeout), (override));
	MOCK_METHOD(void*, dequeue, (), (override));
	MOCK_METHOD(void*, dequeue, (int millisecondsTimeout), (override));
};

TEST(ProducerTest, ProducerCallsGeneratorAndSharedQueue)
{ // When we call produce method then it calls generator and tries to enqueue its value
	// Arrange
	MockGenerator mockGenerator;
	MockQueue mockQueue;
	Producer producer(mockGenerator, mockQueue);

	int VALUE = 12345;

	// Assert
	InSequence seq; 
	EXPECT_CALL(mockGenerator, get()).WillRepeatedly(Return(&VALUE));
	EXPECT_CALL(mockQueue, enqueue(&VALUE, 0));

	// Act
	producer.produce();
}

TEST(ProducerTest, TimeoutedProducerCallsSharedQueueWithCorrectTimeout)
{ // When we call produceTimeout method then it calls generator and tries to enqueue its value for a specified time
	// Arrange
	MockGenerator mockGenerator;
	MockQueue mockQueue;
	Producer producer(mockGenerator, mockQueue);
	const uint32_t TIMEOUT = 10;

	int32_t VALUE = 12345;

	// Assert
	EXPECT_CALL(mockGenerator, get()).WillRepeatedly(Return(&VALUE));
	EXPECT_CALL(mockQueue, enqueue(&VALUE, TIMEOUT));

	// Act
	producer.produceTimeout(TIMEOUT);
}

class SuspendingGenerator : public DataGenerator
{
public:
	void* get()
	{
		static bool isInvoked = false;
		if (isInvoked)
		{
			wasEnteredTwice = true;
		}
		else
		{
			isInvoked = true;
			std::this_thread::sleep_for(std::chrono::milliseconds(10));
			isInvoked = false;
		}
		return nullptr;
	}

	bool wasEnteredTwice = false;
};

TEST(ProducerTest, ProducerCantEnterTwiceIntoGenerator)
{ // When one thread is suspended because of generator then another thread doesn't try to call the generator
	// Arrange
	SuspendingGenerator suspendingGenerator;
	NiceMock<MockQueue> mockQueue;
	Producer producer1(suspendingGenerator, mockQueue);
	Producer producer2(suspendingGenerator, mockQueue);

	// Act
	std::thread thread1(&Producer::produce, &producer1);
	std::thread thread2(&Producer::produce, &producer2);

	thread1.join();
	thread2.join();

	ASSERT_FALSE(suspendingGenerator.wasEnteredTwice);
}
