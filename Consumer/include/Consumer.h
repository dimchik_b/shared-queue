#ifndef __CONSUMER__
#define __CONSUMER__

#include <mutex>
#include "SharedQueue.h"
#include "Actor.h"

class Consumer
{
public:
	Consumer(Actor& theActor, SharedQueue<void>& theQueue) :
		actor(theActor),
		queue(theQueue)
	{}
	~Consumer() {};
	void consume();
	void consumeTimeout(uint32_t timeout_ms);

private:
	Actor& actor;
	SharedQueue<void>& queue;
	static std::mutex mutex;
};

#endif // __CONSUMER__