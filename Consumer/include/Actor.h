#ifndef __ACTOR__
#define __ACTOR__

class Actor
{
public:
	virtual void use(void* data) = 0;
};

class SimpleActor : public Actor
{
public:
	virtual void use(void* data);
};

#endif __ACTOR__
