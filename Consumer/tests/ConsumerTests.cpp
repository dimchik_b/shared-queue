#include <stdint.h>
#include "gtest/gtest.h"
#include "gmock/gmock.h"
#include "Consumer.h"
#include <thread>

using ::testing::_;
using ::testing::Return;
using ::testing::NiceMock;
using ::testing::InSequence;

class MockActor : public Actor
{
public:
	MOCK_METHOD(void, use, (void*), (override));
};

class MockQueue : public SharedQueue<void>
{
public:
	MOCK_METHOD(int, count, (), (const, override));
	MOCK_METHOD(void, enqueue, (void* item), (override));
	MOCK_METHOD(bool, enqueue, (void* item, int millisecondsTimeout), (override));
	MOCK_METHOD(void*, dequeue, (), (override));
	MOCK_METHOD(void*, dequeue, (int millisecondsTimeout), (override));
};

TEST(ConsumerTest, ConsumerCallsSharedQueueAndActor)
{ // When we call consume method then it dequeues a value and calls actor with this value
	// Arrange
	MockActor mockActor;
	MockQueue mockQueue;
	Consumer consumer(mockActor, mockQueue);

	int VALUE = 12345;

	// Assert
	InSequence seq;
	EXPECT_CALL(mockQueue, dequeue()).WillRepeatedly(Return(&VALUE));
	EXPECT_CALL(mockActor, use(&VALUE));

	// Act
	consumer.consume();
}

TEST(ConsumerTest, ConsumerCallsActorWithNullptr)
{ // When we call consume method without timeout then it calls actor even if queue has returned nullptr
  // This is correct because returning of nullptr from non-timeouted dequeue means that somebody has
  // put this value to the queue before
	// Arrange
	MockActor mockActor;
	MockQueue mockQueue;
	Consumer consumer(mockActor, mockQueue);

	// Assert
	InSequence seq;
	EXPECT_CALL(mockQueue, dequeue()).WillRepeatedly(Return(nullptr));
	EXPECT_CALL(mockActor, use(nullptr));

	// Act
	consumer.consume();
}

TEST(ConsumerTest, ConsumerUnsuccessfullyCallsSharedQueue)
{ // When we call timeouted consume method and it returns by the timeout then we shouldn't call actor because of no value
	// Arrange
	MockActor mockActor;
	MockQueue mockQueue;
	Consumer consumer(mockActor, mockQueue);
	const uint32_t TIMEOUT = 10;

	// Assert
	InSequence seq;
	EXPECT_CALL(mockQueue, dequeue(TIMEOUT)).WillRepeatedly(Return(nullptr));
	EXPECT_CALL(mockActor, use(_)).Times(0);

	// Act
	consumer.consumeTimeout(TIMEOUT);
}

TEST(ConsumerTest, ConsumerCallsSharedQueueWithTimeoutAndActor)
{ // When we call timeouted consume method then it dequeues a value and calls actor with this value
	// Arrange
	MockActor mockActor;
	MockQueue mockQueue;
	Consumer consumer(mockActor, mockQueue);
	const uint32_t TIMEOUT = 10;

	int VALUE = 12345;

	// Assert
	InSequence seq;
	EXPECT_CALL(mockQueue, dequeue(TIMEOUT)).WillRepeatedly(Return(&VALUE));
	EXPECT_CALL(mockActor, use(&VALUE));

	// Act
	consumer.consumeTimeout(TIMEOUT);
}

class SuspendingActor : public Actor
{
public:
	void use(void*)
	{
		static bool isInvoked = false;
		if (isInvoked)
		{
			wasEnteredTwice = true;
		}
		else
		{
			isInvoked = true;
			std::this_thread::sleep_for(std::chrono::milliseconds(10));
			isInvoked = false;
		}
	}

	bool wasEnteredTwice = false;
};

TEST(ConsumerTest, ConsumerCantEnterTwiceIntoActor)
{ // When one thread is suspended because of actor work then another thread doesn't try to call the actor
	// Arrange
	SuspendingActor suspendingActor;
	NiceMock<MockQueue> mockQueue;
	Consumer consumer1(suspendingActor, mockQueue);
	Consumer consumer2(suspendingActor, mockQueue);

	// Act
	std::thread thread1(&Consumer::consume, &consumer1);
	std::thread thread2(&Consumer::consume, &consumer2);

	thread1.join();
	thread2.join();

	ASSERT_FALSE(suspendingActor.wasEnteredTwice);
}
