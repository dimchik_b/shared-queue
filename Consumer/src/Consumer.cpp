#include "Consumer.h"

std::mutex Consumer::mutex;

void Consumer::consume()
{
	void* data = queue.dequeue();
	std::unique_lock<std::mutex> lock(mutex);
	actor.use(data);
}

void Consumer::consumeTimeout(uint32_t timeout_ms)
{
	void* data = queue.dequeue(timeout_ms);
	if (data != nullptr)
	{
		std::unique_lock<std::mutex> lock(mutex);
		actor.use(data);
	}
}