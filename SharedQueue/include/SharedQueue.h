#ifndef __SHARED_QUEUE__
#define __SHARED_QUEUE__

/// Thread-safe fixed-size multiple threads shared queue.
template <typename T> class SharedQueue
{
public:

	/// Gets the number of elements contained in the Queue.
	virtual int count() const = 0;

	/// Puts the item into the queue.
	///
	/// @note if the queue is full then this method blocks until there is the room for the item again.
	virtual void enqueue(T* item) = 0;

	/// Puts the item into the queue.
	///
	/// @param millisecondsTimeout Numbers of millieconds to wait.
	///
	/// @return 'true' if the operation was complited successfully, 'false' if the operation timed out.
	///
	/// @note if the queue is full then this method blocks until there is the room for the item again or the operation timed out.
	virtual bool enqueue(T* item, int millisecondsTimeout) = 0;

	/// Removes and returns the item at the beginning of the Queue.
	///
	/// @note if the queue is empty then this method blocks until there is an item again.
	virtual T* dequeue() = 0;

	/// Removes and returns the item at the beginning of the Queue.
	///
	/// @param millisecondsTimeout Numbers of millieconds to wait.
	///
	/// @returns The item at the betting of the Queue or NULL if the operation timed out.
	///
	/// @note if the queue is empty then this method blocks until there is an item again or the operation timed out.
	virtual T* dequeue(int millisecondsTimeout) = 0;
};

#endif // __SHARED_QUEUE__
