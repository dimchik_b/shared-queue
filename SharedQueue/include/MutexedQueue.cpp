#include "MutexedQueue.h"

template <typename T> 
MutexedQueue<T>::MutexedQueue(size_t size) :
	dequeueIndex(0),
	elementsNumber(0)
{
	queue.resize(size);
}

template <typename T> 
int MutexedQueue<T>::count() const 
{
	std::unique_lock<std::mutex> lock(mutex);
	return static_cast<int>(elementsNumber); 
}

template <typename T>
void MutexedQueue<T>::enqueue(T* item)
{
	std::unique_lock<std::mutex> lock(mutex);
	condition.wait(lock, [this]() { return elementsNumber < queue.size(); });

	enqueue_internal(item);
}

template <typename T> 
bool MutexedQueue<T>::enqueue(T* item, int millisecondsTimeout) 
{ 
	std::unique_lock<std::mutex> lock(mutex);
	bool canEnqueue = condition.wait_for(
		lock, 
		std::chrono::milliseconds(millisecondsTimeout), 
		[this]() { return elementsNumber < queue.size(); }
	);
	
	if (canEnqueue)
	{
		enqueue_internal(item);
	}
	return canEnqueue;
}

template <typename T>
T* MutexedQueue<T>::dequeue()
{
	std::unique_lock<std::mutex> lock(mutex);
	condition.wait(lock, [this]() {return elementsNumber > 0; });
	return dequeue_internal();
}

template <typename T> 
T* MutexedQueue<T>::dequeue(int millisecondsTimeout) 
{ 
	std::unique_lock<std::mutex> lock(mutex);
	bool canDequeue = condition.wait_for(
		lock,
		std::chrono::milliseconds(millisecondsTimeout),
		[this]() {return elementsNumber > 0; }
	);

	T* item = nullptr;
	if(canDequeue)
	{
		item = dequeue_internal();
	}
	return item;
}

template <typename T>
void MutexedQueue<T>::enqueue_internal(T* item)
{
	// Enqueue point is elements number ahead the dequeue point 
	size_t enqueueIndex = (dequeueIndex + elementsNumber) % queue.size();
	queue.at(enqueueIndex) = item;
	elementsNumber++;
	condition.notify_all(); // unfreeze those who may wait for the new element
}

template<typename T>
T* MutexedQueue<T>::dequeue_internal()
{
	T* item = queue.at(dequeueIndex);
	elementsNumber--;
	moveIndex(dequeueIndex);
	condition.notify_one(); // unfreeze those who may wait for free space

	return item;
}

template <typename T> 
void MutexedQueue<T>::moveIndex(size_t &indexToMove)
{
	indexToMove++;
	if (indexToMove >= queue.size())
	{
		indexToMove = 0;
	}
}
