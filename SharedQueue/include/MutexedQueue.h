#ifndef __MUTEXED_QUEUE__
#define __MUTEXED_QUEUE__

#include <vector>
#include <mutex>
#include <condition_variable>
#include "SharedQueue.h"

/// Thread-safe fixed-size multiple threads shared queue.
template <typename T> class MutexedQueue : public SharedQueue<T>
{
public:
	MutexedQueue(size_t size);

	int count() const override;
	void enqueue(T* item) override;
	bool enqueue(T* item, int millisecondsTimeout) override;
	T* dequeue() override;
	T* dequeue(int millisecondsTimeout) override;

private:
	std::vector<T*> queue;
	size_t dequeueIndex;
	size_t elementsNumber;
	mutable std::mutex mutex;
	std::condition_variable condition;

	void moveIndex(size_t &currentIndex);
	void enqueue_internal(T* item);
	T* dequeue_internal();
};

#include "MutexedQueue.cpp"
#endif // __MUTEXED_QUEUE__
