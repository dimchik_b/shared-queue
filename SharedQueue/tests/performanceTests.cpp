#include <algorithm>
#include <stdint.h>
#include <thread>
#include <deque>
#include "gtest/gtest.h"
#include "MutexedQueue.h"
#include "Consumer.h"
#include "Producer.h"

const uint32_t TURN_NUMBER = 100;

typedef decltype(std::chrono::steady_clock::now()) TimePoint;
typedef std::chrono::duration<double, std::micro> Duration_us;
using std::ref;
using namespace std::chrono_literals;

TEST(PerformanceTest, MinimumAverageMaximumWithClasses)
{ // Finding out minimum, average and maximum latencies for the data transfer with producer/consumer classes
	Duration_us shortest = Duration_us::max();
	Duration_us longest = Duration_us::min();
	Duration_us average = Duration_us::zero();

	for (uint32_t i = 0; i < TURN_NUMBER; i++)
	{
		SerialGenerator generator;
		SimpleActor actor;
		MutexedQueue<void> queue(10);
		Producer producer(generator, queue);
		Consumer consumer(actor, queue);
		TimePoint start;
		TimePoint end;
		bool successful = false;

		std::thread producerThread([&]()
			{
				successful = false;
				if (queue.count() == 0)
				{
					start = std::chrono::steady_clock::now();
					producer.produce();
					successful = true;
				}
			}
		);

		std::thread consumerThread([&]()
			{
				consumer.consume();
				end = std::chrono::steady_clock::now();
			}
		);

		producerThread.join();
		consumerThread.join();

		ASSERT_TRUE(successful) << "Queue was not empty at the " << i << " turn";
		Duration_us currentDuration = end - start;
		shortest = std::min(shortest, currentDuration);
		longest = std::max(longest, currentDuration);
		average += currentDuration / TURN_NUMBER;

		average += currentDuration / TURN_NUMBER;
	}

	std::cout << "Shortest latency was " << shortest.count() << " us" << std::endl;
	std::cout << "Average latency was " << average.count() << " us" << std::endl;
	std::cout << "Longest latency was " << longest.count() << " us" << std::endl;
}

TEST(PerformanceTest, MinimumAverageMaximumWithoutClasses)
{ // Finding out minimum, average and maximum latencies for the data transfer without special classes to feed/consume data
	Duration_us shortest = Duration_us::max();
	Duration_us longest = Duration_us::min();
	Duration_us average = Duration_us::zero();

	for (uint32_t i = 0; i < TURN_NUMBER; i++)
	{
		MutexedQueue<void> queue(10);
		TimePoint start;
		TimePoint end;
		bool successful = false;

		std::thread producerThread([&]()
			{
				successful = false;
				if (queue.count() == 0)
				{
					void* dummy = reinterpret_cast<void*>(1);
					start = std::chrono::steady_clock::now();
					queue.enqueue(dummy);
					successful = true;
				}
			}
		);

		std::thread consumerThread([&]()
			{
				void* dummy = queue.dequeue();
				end = std::chrono::steady_clock::now();
			}
		);

		producerThread.join();
		consumerThread.join();

		ASSERT_TRUE(successful) << "Queue was not empty at the " << i << " turn";

		Duration_us currentDuration = end - start;
		shortest = std::min(shortest, currentDuration);
		longest = std::max(longest, currentDuration);
		average += currentDuration / TURN_NUMBER;
	}

	std::cout << "Shortest latency was " << shortest.count() << " us" << std::endl;
	std::cout << "Average latency was " << average.count() << " us" << std::endl;
	std::cout << "Longest latency was " << longest.count() << " us" << std::endl;
}

TEST(PerformanceTest, MinimumAverageMaximumWithoutKillingThreadsOnEveryTurn)
{ // Finding out minimum, average and maximum latencies for the data transfer when only the 
  // producing thread and the consuming thread live during all the test
	TimePoint start[TURN_NUMBER];
	TimePoint end[TURN_NUMBER];

	MutexedQueue<void> queue(10);

	std::thread producerThread([&]()
		{
			for(size_t i = 0; i < TURN_NUMBER; i++)
			{
				void* dummy = reinterpret_cast<void*>(1);
				while (queue.count())
				{
					std::this_thread::yield();
				}
				start[i] = std::chrono::steady_clock::now();
				queue.enqueue(dummy);
			}
		}
	);

	std::thread consumerThread([&]()
		{
			for (size_t i = 0; i < TURN_NUMBER; i++)
			{
				void* dummy = queue.dequeue();
				end[i] = std::chrono::steady_clock::now();
			}
		}
	);

	producerThread.join();
	consumerThread.join();

	Duration_us shortest = Duration_us::max();
	Duration_us longest = Duration_us::min();
	Duration_us average = Duration_us::zero();
	for (uint32_t i = 0; i < TURN_NUMBER; i++)
	{
		Duration_us currentDuration = end[i] - start[i];
		shortest = std::min(shortest, currentDuration);
		longest = std::max(longest, currentDuration);
		average += currentDuration / TURN_NUMBER;
	}

	std::cout << "Shortest latency was " << shortest.count() << " us" << std::endl;
	std::cout << "Average latency was " << average.count() << " us" << std::endl;
	std::cout << "Longest latency was " << longest.count() << " us" << std::endl;
}

typedef int DataType;
void produceFunction(size_t turnNumber, MutexedQueue<DataType>& queue, size_t threadNumber)
{
	for (size_t j = 0; j < turnNumber; j++)
	{
		DataType* p_item = new DataType;
		if (!queue.enqueue(p_item, 1000))
		{
			// if we can't enqueue for some reason we print the diagnostics and try again
			std::ostringstream out;
			out << "Can't enqueue in time. thread " << threadNumber << ", turn " << j << "\r\n";
			std::cout << out.str();

			j--; // we have to repeat this unsuccessful trial
		}
	}
}

void consumerFunction(size_t turnNumber, MutexedQueue<DataType>& queue)
{
	DataType* p_item;
	for (size_t i = 0; i < turnNumber; i++)
	{
		p_item = queue.dequeue();
		if (p_item)
		{
			delete p_item;
		}
		else
		{
			std::cout << "NULL received" << std::endl;
		}
	}
}

struct EntitiesNumber
{
	size_t producers;
	size_t consumers;
};

class StressTest : public ::testing::TestWithParam<EntitiesNumber>
{

};

TEST_P(StressTest, MProducerNConsumer)
{ // This test checks correct SharedQueue behavior when there are a lot of producers and consumers
	const size_t QUEUE_SIZE = 10;
	const EntitiesNumber number = GetParam();
	std::deque<std::unique_ptr<std::thread>> producerThreads;
	std::deque<std::unique_ptr<std::thread>> consumerThreads;
	MutexedQueue<DataType> queue(QUEUE_SIZE);

	size_t producerTurnNumber = TURN_NUMBER;
	for (size_t i = 0; i < number.producers; i++)
	{
		producerThreads.push_front(std::unique_ptr<std::thread>(new std::thread(produceFunction, producerTurnNumber, std::ref(queue), i)));
	}

	size_t consumerTurnNumber = number.producers * TURN_NUMBER / number.consumers;
	for (size_t i = 0; i < number.consumers; i++)
	{
		// If the number of the data pieces isn't multiple of the consumer number the last consumer consumes also the reminder
		if (i == number.consumers - 1)
		{
			consumerTurnNumber += (number.producers * TURN_NUMBER) % number.consumers;
		}

		consumerThreads.push_front(std::unique_ptr<std::thread>(new std::thread(consumerFunction, consumerTurnNumber, std::ref(queue))));
	}

	for (auto&& currentThread : producerThreads)
	{
		currentThread->join();
	}
	producerThreads.clear();

	for (auto&& currentThread : consumerThreads)
	{
		currentThread->join();
	}
	consumerThreads.clear();

	ASSERT_EQ(queue.count(), 0);
}

INSTANTIATE_TEST_SUITE_P(
	// Here we create variants of the producers and consumers number for the stress test
	Stress, StressTest, 
	::testing::Values(EntitiesNumber{ 50, 100 }, EntitiesNumber{ 100, 50 }),
	[](const testing::TestParamInfo<StressTest::ParamType>& info)->std::string
	{
		return std::to_string(info.param.producers) + "producers" + 
			std::to_string(info.param.consumers) + "consumers";
	}
	);
