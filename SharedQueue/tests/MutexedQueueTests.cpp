#include <stdint.h>
#include "gtest/gtest.h"
#include "MutexedQueue.h"
#include <thread>

typedef int32_t DataType;

TEST(MutexedQueue, EnqueueSizeItems)
{ // When we enqueue items then we can enqueue only size items
	//Arrange
	int32_t QUEUE_SIZE = 10;
	MutexedQueue<DataType> mq(QUEUE_SIZE);

	//Act and assert
	bool success = false;
	for (int32_t i = 0; i < QUEUE_SIZE; i++)
	{
		//Act
		//Assert
		ASSERT_TRUE(mq.enqueue(reinterpret_cast<DataType*>(&i), 0)) << "Enqueuing of " << i << "-th element was unsuccessful";
	}
	//Act
	//Assert
	ASSERT_FALSE(mq.enqueue(reinterpret_cast<DataType*>(&QUEUE_SIZE), 0)) << "Enqueuing of " << QUEUE_SIZE << "-th element mustn't be successful";
}

TEST(MutexedQueue, DequeueFromEmptyQueue)
{ // When we dequeue items then we can dequeue no items from the empty queue
	//Arrange
	const size_t QUEUE_SIZE = 10;
	MutexedQueue<DataType> mq(QUEUE_SIZE);

	//Act
	DataType* p_item = mq.dequeue(0);

	//Assert
	ASSERT_EQ(p_item, nullptr) << "Dequeuing from an empty queue mustn't be successfull";
}

TEST(MutexedQueue, DequeueSizeItems)
{ // When we dequeue items from the full queue then we can dequeue size items
	//Arrange
	const size_t QUEUE_SIZE = 10;
	MutexedQueue<DataType> mq(QUEUE_SIZE);
	for (int32_t i = 0; i < QUEUE_SIZE; i++)
	{
		DataType* p_item = new DataType(i);
		mq.enqueue(p_item, 0);
	}

	DataType* p_item = nullptr;
	for (int32_t i = 0; i < QUEUE_SIZE; i++)
	{
		//Act
		p_item = mq.dequeue(0);

		//Assert
		ASSERT_NE(p_item, nullptr) << "Dequeuing of " << i << "-th element was unsuccessful";
		ASSERT_EQ(*p_item, DataType(i)) << i << "-th element was wrong";
		delete p_item;
	}

	//Act
	p_item = mq.dequeue(0);

	//Assert
	if (p_item != nullptr)
	{
		delete p_item;
		FAIL() << "Dequeuing of " << QUEUE_SIZE << "-th element mustn't be successful";
	}
}

TEST(MutexedQueue, DequeueFewItems)
{ // When we dequeue items from nonfull queue then we can dequeue as many items as we put
	//Arrange
	const size_t QUEUE_SIZE = 10;
	const size_t ELEMENTS_TO_ENQUEUE = QUEUE_SIZE - 3;
	MutexedQueue<DataType> mq(QUEUE_SIZE);
	for (int32_t i = 0; i < ELEMENTS_TO_ENQUEUE; i++)
	{
		DataType* p_item = new DataType(i);
		mq.enqueue(p_item, 0);
	}

	DataType* p_item = nullptr;
	for (int32_t i = 0; i < ELEMENTS_TO_ENQUEUE; i++)
	{
		//Act
		p_item = mq.dequeue(0);

		//Assert
		ASSERT_NE(p_item, nullptr) << "Dequeuing of " << i << "-th element was unsuccessful";
		ASSERT_EQ(*p_item, DataType(i)) << i << "-th element was wrong";
		delete p_item;
	}

	//Act
	p_item = mq.dequeue(0);

	//Assert
	if (p_item != nullptr)
	{
		delete p_item;
		FAIL() << "Dequeuing of " << ELEMENTS_TO_ENQUEUE << "-th element mustn't be successful";
	}
}

TEST(MutexedQueue, DequeueSizeItems2)
{ // Given an empty queue in a noninitial state. 
  // When we enqueue size items then we can dequeue size items
	//Arrange
	const size_t QUEUE_SIZE = 10;
	const size_t ELEMENTS_TO_ENQUEUE = QUEUE_SIZE - 3;
	MutexedQueue<DataType> mq(QUEUE_SIZE);
	for (int32_t i = 0; i < ELEMENTS_TO_ENQUEUE; i++)
	{
		mq.enqueue(reinterpret_cast<DataType*>(&i), 0);
		mq.dequeue(0);
	}

	for (int32_t i = 0; i < QUEUE_SIZE; i++)
	{
		DataType* p_item = new DataType(i);
		mq.enqueue(p_item, 0);
	}

	DataType* p_item = nullptr;
	for (int32_t i = 0; i < QUEUE_SIZE; i++)
	{
		//Act
		p_item = mq.dequeue(0);

		//Assert
		ASSERT_NE(p_item, nullptr) << "Dequeuing of " << i << "-th element was unsuccessful";
		ASSERT_EQ(*p_item, DataType(i)) << i << "-th element was wrong";
		delete p_item;
		p_item = nullptr;
	}

	//Act
	p_item = mq.dequeue(0);

	//Assert
	if (p_item != nullptr)
	{
		delete p_item;
		FAIL() << "Dequeuing of " << QUEUE_SIZE << "-th element mustn't be successful";
	}
}

TEST(MutexedQueue, OneProducerOneConsumer)
{ // When producer and consumer work in different threads, then the quwue works correctly
	const size_t QUEUE_SIZE = 10;
	const int32_t TURN_NUMBER = 100;
	MutexedQueue<DataType> mq(QUEUE_SIZE);

	std::thread producer1([TURN_NUMBER, &mq]()
		{
			for (int32_t i = 0; i < TURN_NUMBER; i++)
			{
				DataType* p_item = new DataType(i);
				mq.enqueue(p_item);
			}
		}
	);
	std::thread consumer1([TURN_NUMBER, &mq]()
		{
			DataType* p_item;
			for (int32_t i = 0; i < TURN_NUMBER; i++)
			{
				p_item = mq.dequeue();
				if (p_item)
				{
					delete p_item;
				}
				else
				{
					std::cout << "NULL" << std::endl;
				}
			}
		}
	);
	producer1.join();
	consumer1.join();
	ASSERT_EQ(mq.count(), 0);
}

TEST(MutexedQueue, TimedEnqueuePutsInTime)
{ // When we use timeouted enqueue and there is a free space in the queue then enqueuing goes ~immediately
	//Arrange
	const int32_t QUEUE_SIZE = 10;
	const int32_t TIMEOUT = 10;
	MutexedQueue<DataType> mq(QUEUE_SIZE);

	//Act and assert
	bool success = false;
	for (int32_t i = 0; i < QUEUE_SIZE; i++)
	{
		//Act
		auto start = std::chrono::steady_clock::now();
		mq.enqueue(reinterpret_cast<DataType*>(&i), TIMEOUT);
		std::chrono::duration<double, std::milli> diff = std::chrono::steady_clock::now() - start;

		//Assert
		ASSERT_GE(diff.count(), 0) << "Enqueuing duration was " << diff.count() << " ms instead of 0 ms";
	}
}

TEST(MutexedQueue, TimedEnqueueCantPut)
{ // When we use timeouted enqueue and no free space in the queue then we timeout in a time
	//Arrange
	const size_t QUEUE_SIZE = 10;
	const int32_t TIMEOUT = 10;
	MutexedQueue<DataType> mq(QUEUE_SIZE);
	for (int32_t i = 0; i < QUEUE_SIZE; i++)
	{
		mq.enqueue(reinterpret_cast<DataType*>(&i));
	}

	//Act
	auto start = std::chrono::steady_clock::now();
	DataType data = 0;
	mq.enqueue(reinterpret_cast<DataType*>(&data), TIMEOUT);
	std::chrono::duration<double, std::milli> diff = std::chrono::steady_clock::now() - start;

	//Assert
	ASSERT_GE(diff.count(), TIMEOUT) << "Enqueuing duration was " << diff.count() << " ms instead of " << TIMEOUT << " ms";
}

TEST(MutexedQueue, TimedDequeueCanGet)
{ // When we use timeouted dequeue and there is data in the queue then we dequeue ~immediately
	//Arrange
	const size_t QUEUE_SIZE = 10;
	const int32_t TIMEOUT = 10;
	MutexedQueue<DataType> mq(QUEUE_SIZE);
	DataType data = 0;
	mq.enqueue(&data);

	//Act
	auto start = std::chrono::steady_clock::now();
	DataType* p_item = nullptr;
	p_item = mq.dequeue(TIMEOUT);
	std::chrono::duration<double, std::milli> diff = std::chrono::steady_clock::now() - start;

	//Assert
	ASSERT_EQ(p_item, &data) << "Received pointer is wrong: " << p_item;
	ASSERT_GE(diff.count(), 0) << "Enqueuing duration was " << diff.count() << " ms instead of 0 ms";
}

TEST(MutexedQueue, TimedDequeueCantGet)
{ // When we use timeouted dequeue and no data in the queue then we timeout in a time
	//Arrange
	const size_t QUEUE_SIZE = 10;
	const int32_t TIMEOUT = 10;
	MutexedQueue<DataType> mq(QUEUE_SIZE);

	//Act
	auto start = std::chrono::steady_clock::now();
	DataType* p_item = nullptr;
	p_item = mq.dequeue(TIMEOUT);
	std::chrono::duration<double, std::milli> diff = std::chrono::steady_clock::now() - start;

	//Assert
	ASSERT_GE(diff.count(), TIMEOUT) << "Enqueuing duration was " << diff.count() << " ms instead of " << TIMEOUT << " ms";
}

TEST(MutexedQueue, CountForEmptyQueue)
{ // When the queue is empty then count returns 0
	//Arrange
	const size_t QUEUE_SIZE = 10;
	MutexedQueue<DataType> mq(QUEUE_SIZE);

	//Act
	int count = mq.count();

	//Assert
	ASSERT_EQ(count, 0) << "Count isn't 0 but " << count;
}

TEST(MutexedQueue, CountForNonemptyQueue)
{ // When the queue isn't empty then count returns correct number of items
	//Arrange
	const size_t QUEUE_SIZE = 10;
	const size_t ELEMENTS_TO_ENQUEUE = QUEUE_SIZE - 3;
	MutexedQueue<DataType> mq(QUEUE_SIZE);
	for (int32_t i = 0; i < ELEMENTS_TO_ENQUEUE; i++)
	{
		mq.enqueue(reinterpret_cast<DataType*>(&i));
	}

	//Act
	int count = mq.count();

	//Assert
	ASSERT_EQ(count, ELEMENTS_TO_ENQUEUE) << "Count isn't " << ELEMENTS_TO_ENQUEUE << " but " << count;
}

TEST(MutexedQueue, CountForFullQueue)
{ // When the queue is full then count returns correct number of items
	//Arrange
	const size_t QUEUE_SIZE = 10;
	MutexedQueue<DataType> mq(QUEUE_SIZE);
	for (int32_t i = 0; i < QUEUE_SIZE; i++)
	{
		mq.enqueue(reinterpret_cast<DataType*>(&i));
	}

	//Act
	int count = mq.count();

	//Assert
	ASSERT_EQ(count, QUEUE_SIZE) << "Count isn't " << QUEUE_SIZE << " but " << count;
}

TEST(MutexedQueue, CountForQueueWithHistory)
{ // When the queue was filled and emptied and then filled again then count returns correct number of items
	//Arrange
	const size_t QUEUE_SIZE = 10;
	const size_t ELEMENTS_TO_ENQUEUE = QUEUE_SIZE - 3;
	MutexedQueue<DataType> mq(QUEUE_SIZE);

	for (int32_t i = 0; i < QUEUE_SIZE; i++)
	{
		mq.enqueue(reinterpret_cast<DataType*>(&i));
	}
	for (int32_t i = 0; i < QUEUE_SIZE; i++)
	{
		DataType* p_item = mq.dequeue();
	}

	for (int32_t i = 0; i < ELEMENTS_TO_ENQUEUE; i++)
	{
		mq.enqueue(reinterpret_cast<DataType*>(&i));
	}

	//Act
	int count = mq.count();

	//Assert
	ASSERT_EQ(count, ELEMENTS_TO_ENQUEUE) << "Count isn't " << ELEMENTS_TO_ENQUEUE << " but " << count;
}
